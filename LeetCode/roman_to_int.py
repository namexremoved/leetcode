
# not complete

class Solution:
    def romanToInt(self, s: str) -> int:

        cypher = {'I': 1,
                  'V': 5,
                  'X': 10,
                  'L': 50,
                  'C': 100,
                  'D': 500,
                  'M': 1000}

        count = 0
        l = []
        while count < len(s):
            if count != len(s):
                if cypher[s[count]] < cypher[s[count + 1]]:
                    l.append(cypher[s[count + 1]] - cypher[s[count]])
                    count += 2
                else:
                    l.append(cypher[s[count]])
                count += 1

        total = sum(l)
        return total

