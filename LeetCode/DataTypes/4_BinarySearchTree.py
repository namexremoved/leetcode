class Node:
    def __init__(self, data, left=None, right=None):
        self.data = data
        self.left = left
        self.right = right


# Left = Less than
# Right = Greater than

class Tree:
    def __init__(self, data, left=None, right=None):
        self.data = data
        self.left = left
        self.right = right

    def insert(self, data):
        if self.data == data:
            return False  # Duplicate data
        elif self.data > data:
            if self.left is not None:
                return self.left.insert(data)  # Run Recursive to hit the bottomLeft
            else:
                self.left = Tree(data)  # Add the node itself
                return True  # return confirmation
        else:
            if self.right is not None:
                return self.right.insert(data)  # Run Recursive to hit the bottomRight
            else:
                self.right = Tree(data)  # Add the node itself
                return True  # return confirmation

    def find(self, data):
        if self.data == data:  # if data is on current node, return that data
            return data
        elif self.data > data:  # if current node.data is greater then check left
            if self.left is None:
                return False  # data not in tree
            else:
                return self.left.find(data)  # recursively check left side
        elif self.data < data:  # if if current node.data is less than data then check right
            if self.right is None:
                return False  # data not in tree
            else:
                return self.right.find(data)  # recursively check left side

    def get_size(self):
        if self.left is not None and self.right is not None:
            return 1 + self.left.get_size() + self.right.get_size()
        elif self.left:
            return 1 + self.left.get_size()
        elif self.right:
            return 1 + self.right.get_size()
        else:
            return 1

    def preorder(self):  # DFS
        if self is not None:
            print(self.data, end=" ")  # printing before left side
            if self.left is not None:
                self.left.preorder()
            if self.right is not None:
                self.right.preorder()

    def inorder(self):  # BFS
        if self is not None:
            if self.left is not None:
                self.left.inorder()
            print(self.data, end=" ")  # printing after left side
            if self.right is not None:
                self.right.inorder()


######
if __name__ == '__main__':
    tree = Tree(5)
    tree.insert(10)
    for i in [1, 2, 3, 7, 2, 7, 9, 345, 72, 17, 82, 281, 53, 81, 25, 1, 6]:
        tree.insert(i)
    for i in range(20):
        print(tree.find(i), end=' ')
    print('\n', tree.get_size())

    tree.preorder()
    print()
    tree.inorder()
    print()
