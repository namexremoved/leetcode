from collections import deque

my_queue = deque()
my_queue.append('left_side')
my_queue.append('middle')
my_queue.append('right_side')
my_queue.popleft()  # pops left_side


# First-in/First Out (FIFO)

class Queue:
    def __init__(self):
        self.queue = list()

    def enqueue(self, item):
        self.queue.append(item)

    def dequeue(self):
        if len(self.queue) > 0:
            return my_queue.popleft()
        else:
            return None

    def get_size(self):
        return len(self.queue)
