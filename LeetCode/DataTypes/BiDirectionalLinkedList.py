
#   Root: pointer to the beginning of the List
#   Size: number of nodes in the List

# (ends with a None Pointer)
x = [1, 2, 3, 4, 5, 5]

x.count(5)




class Node:
    def __init__(self, data, next=None, previous=None):
        self.data = data
        self.next_node = next
        self.prev_node = previous

    def __str__(self):
        return '(' + str(self.data) + ')'


class BiDirectionalLinkedList:
    def __init__(self, r=None):
        self.root = r
        self.size = 0

    def add(self, d):
        new_node = Node(data=d, next=self.root)
        self.root = new_node
        self.size += 1

    def find(self, d):
        this_node = self.root
        while this_node is not None:
            if this_node.data == d:
                return d
            else:
                this_node = this_node.next_node
        return None

    def remove(self, d):
        this_node = self.root
        prev_node = None

        while this_node is not None:
            if this_node.data == d:
                pass ##

        while this_node is not None:
            if this_node.data == d:
                if prev_node is not None:  # data is not in the root node
                    prev_node.next_node = this_node.next_node
                else:  # data is in root node
                    self.root = this_node.next_node
                self.size -= 1
                return True  # data removed
            else:  # go to next node
                prev_node = this_node
                this_node = this_node.next_node
        return False  # data not found


    def print_list(self):
        this_node = self.root
        while this_node is not None:
            print(this_node, end='->')
            this_node = this_node.next_node
        print('None')
