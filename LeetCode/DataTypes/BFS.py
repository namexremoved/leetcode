class Solution:
    def numIslands(self, grid: List[List[str]]) -> int:

        grid_length = len(grid[0])
        grid_height = len(grid)

        island_count = 0
        LAND = "1"
        WATER = "0"

        def backtrack(x, y):
            if (
                    x < 0 or x >= grid_length or
                    y < 0 or y >= grid_height or
                    grid[y][x] == WATER
            ): return

            grid[y][x] = WATER
            backtrack(x + 1, y)
            backtrack(x - 1, y)
            backtrack(x, y + 1)
            backtrack(x, y - 1)

        for y in range(grid_height):
            for x in range(grid_length):
                if grid[y][x] == LAND:
                    island_count += 1
                    backtrack(x, y)

        return island_count