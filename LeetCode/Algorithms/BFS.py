
# Graph Traversal Algorithm
# Finds Every Vertex that is reachable from the source and finds the distance from the source
# - distance = number of Edges it has to traverse
# works on directed and undirected graphs.
# O(V+E)


# Discovery status = Color
# Distance from source = d
# Predecessor vertex = pi

# Colors:
#   Grey = Undiscovered
#   Black = Discovered
#   Blue = Visited

# Uses a Queue (first in first out)


