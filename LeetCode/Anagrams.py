
def are_anagrams(s1, s2):
    if len(s1) != len(s2):
        return False

    def letter_count(word):
        count = [0] * 26
        for ch in word:
            count[ord(ch) - ord("a")] += 1
        return count

    return letter_count(s1) == letter_count(s2)


# Test Cases:

if __name__ == "__main__":
    print(are_anagrams("asdf", "sdfa"))  # true
    print(are_anagrams("qwertyui", "tryeuwiq"))  # true
    print(are_anagrams("qweui", "treuwiq"))  # false
    print(are_anagrams("poiu", "asdf"))  # false
